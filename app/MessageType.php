<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MessageType extends Model
{
    protected $table = "loaitin";
    protected $fillable = ['idTheLoai', 'Ten', 'TenKhongDau'];

    public function category()
    {
    	return $this->belongsTo('App\MessageType', 'idTheLoai', 'id');
    }

    public function news()
    {
    	return $this->hasMany('App\News', 'idLoaiTin', 'id');
    }
}
