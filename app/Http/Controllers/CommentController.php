<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
class CommentController extends Controller
{
    public function getDeleteComment($id, $idNews)
    {
        $comment = Comment::find($id);
        $comment->delete();
        return redirect('admin/news/edit/'.$idNews)->with('mes_del_cmt','Xóa bình luận thành công.');
    }
}
