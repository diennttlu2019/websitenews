<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Slide;

class SlideController extends Controller
{
    public function getList()
    {
        $slides = Slide::orderBy('id', 'DESC')->get();
        return view('admin.slide.list', compact('slides'));
    }

    public function getAdd()
    {
        return view('admin.slide.add');
    }

    public function postAll(Request $request, $id)
    {
        
    }
    
    public function getEdit($id)
    {
        # code...
    }

    public function postEdit(Request $request, $id)
    {
        
    }

    public function getDelete($id)
    {

    }
}
