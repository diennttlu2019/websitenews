<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MessageTypeCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'Ten' => 'required|min:3|max:100|unique:loaitin,Ten',
			'idTheLoai' => 'required' 
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'idTheLoai.required' => 'Bạn chưa chọn thể loại',
            'Ten.required' => 'Bạn chưa nhập tên Loại tin',
            'Ten.unique' => 'Loại tin đã tồn tại.',
            'Ten.min' => 'Loại tin phải có độ dài từ 3 đến 100 ký tự',
            'Ten.max'=> 'Loại tin phải có độ dài từ 3 đến 100 ký tự'
        ];
    }
}
