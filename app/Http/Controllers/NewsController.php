<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\News;
use App\Category;
use App\MessageType;
use App\Comment;
use App\Http\Requests\NewsCreateRequest;

class NewsController extends Controller
{
    public function getList()
    {
        $news = News::orderBy('id', 'DESC')->get();
        return view('admin.news.list', ['news' => $news]);
    }

    public function getAdd()
    {
        $categories = Category::all();
        $message_types = MessageType::all();
        return view('admin.news.add', compact('categories', 'message_types'));
    }

    public function postAdd(NewsCreateRequest $request)
    {
        $image_news = '';
        if ( $request->hasFile('Hinh') ) {
            $file = $request->file('Hinh');
            $last_file = $file->getClientOriginalExtension();
            $nameImage = $file->getClientOriginalName();
            $image = time()."_".$nameImage;
            while (file_exists('upload/tintuc/'.$image)) {
                $image = time()."_".$nameImage;
            }
            $file->move("upload/tintuc", $image);
            $image_news = $image;
        }else {
            $image_news = "";
        }
        $news = News::create([
            'TieuDe'         => $request->TieuDe,
            'TieuDeKhongDau' => changeTitle( $request->TieuDe ),
            'TomTat'         => $request->TomTat,
            'NoiDung'        => $request->NoiDung,
            'idLoaiTin'      => $request->LoaiTin,
            'NoiBat'         => $request->NoiBat,
            'SoLuotXem'      => 0
        ]);
        return redirect()->route('news.add')->with('message', 'Đã thêm thành công.');
    }

    public function getEdit($id)
    {
        $news = News::find($id);
        $categories = Category::all();
        $message_types = MessageType::all();
        // return view('admin/news/edit', ['news' => $news, 'categories' => $categories, 'message_types' => $message_types]);
        return view('admin.news.edit', compact('news', 'categories', 'message_types'));
    }

    public function postEdit(NewsCreateRequest $request, $id)
    {
        $news = News::find($id);
        $image_news = '';
        if ( $request->hasFile('Hinh' )) {
            $file = $request->file('Hinh');
            $nameImage = $file->getClientOriginalName();
            $image = time()."_".$nameImage;
            while (file_exists('upload/tintuc/'.$image)) {
                $image = time()."_".$nameImage;
            }    
            $file->move("upload/tintuc", $image);
            $image_news = $image;
            if ( !empty( $new->Hinh )) {
                if ( file_exists('upload/tintuc/'.$news->Hinh )) {
                    unlink("upload/tintuc/".$news->Hinh);
                }
            }
        }

        $create = News::where('id', $id)->update([
            'TieuDe'    => $request->TieuDe,
            'TieuDeKhongDau'    => changeTitle( $request->TieuDe ),
            'TomTat'        => $request->TomTat,
            'NoiDung'   => $request->NoiDung,
            'idLoaiTin' => $request->LoaiTin,
            'NoiBat'    => $request->NoiBat,
            'Hinh'      => $image_news

        ]);
        return redirect()->back()->with('message', 'Sửa thành công!!');
    }

    public function getDelete($id)
    {
        $news = News::find($id);
        $news->delete();
        return redirect('admin.news.list')->with('message', 'Đã xóa thành công.');
    }
}
