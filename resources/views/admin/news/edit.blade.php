@extends('admin.layout.index')
@section('content')
<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Tin tức
                            <small>{{ $news->TieuDe }}</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-9" style="padding-bottom:120px">
                        @if( count($errors) > 0)
                            <div class="alert alert-danger">
                                @foreach( $errors->all() as $err )
                                    {{ $err }}<br>
                                @endforeach
                            </div>
                        @endif
                        @if( session('message') )
                            <div class="alert alert-success">
                                {{ session('message') }}
                            </div>
                        @endif
                        <form action="{{ route('news.edit', ['id' => $news->id]) }}" method="POST" enctype="multipart/form-data"> 
                            @csrf
                            <div class="form-group">
                                <label>Thể loại</label>
                                <select class="form-control" id="TheLoai">
                                    <option>Chọn thể loại</option>
                                    @foreach( $categories as $cat )
                                        @if( $news->messageType->category->id == $cat->id )
                                           <option value="{{ $cat->id }}" selected="true">{{ $cat->Ten }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Loại tin</label>
                                <select class="form-control" name="LoaiTin" id="LoaiTin">
                                    @foreach( $message_types as $mes ) 
                                        @if( $news->messageType->id == $mes->id )
                                           <option value="{{ $mes->id }}" selected="true">{{ $mes->Ten }}
                                        @else
                                            <option value="{{ $mes->id }}">{{ $mes->Ten }}
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Tiêu đề</label>
                                <input class="form-control" name="TieuDe" value="{{ $news->TieuDe }}"  />
                            </div>
                            <div class="form-group">
                                <label>Tóm tắt</label>
                                <textarea id="TomTat" name="TomTat"  class="form-control ckeditor" rows="4">{{ $news->TomTat }}</textarea>
                            </div>
                            <div class="form-group">
                                <label>Nội dung</label>
                                <textarea id="NoiDung" name="NoiDung"  class="form-control ckeditor" rows="10">{{ $news->NoiDung }}</textarea>
                            </div>
                            
                            <div class="form-group">
                                <label>Hình ảnh</label>
                                <p><img width="400px" height="300px" src="upload/tintuc/{{ $news->Hinh }}" alt=""></p>
                                <input type="file" name="Hinh" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Nổi bật</label>
                                <label class="radio-inline">
                                    
                                    <input name="NoiBat" value="1" @if( $news->NoiBat == 1 ) checked="" @endif type="radio">Có
                                </label>
                                <label class="radio-inline">
                                    <input name="NoiBat" value="0" @if( $news->NoiBat == 0 ) checked="" @endif type="radio">Không
                                </label>
                            </div>
                            <input type="submit" class="btn btn-default" value="Sửa">
                            <input type="reset" class="btn btn-default" value="Làm mới">
                        <form>
                    </div>
                </div>
                <!-- /.row -->
                <div class="row" style="margin-bottom: 50px;">
                    <div class="col-lg-12">
                        <h1 class="page-header">Bình luận
                            <small>Danh sách</small>
                        </h1>
                    </div>
                    @if( session('mes_del_cmt') )
                        <div class="alert alert-success">
                            {{ session('mes_del_cmt') }}
                        </div>
                    @endif
                    <!-- /.col-lg-12 -->
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                <th>ID</th>
                                <th>Người dùng</th>
                                <th>Ngày đăng</th>
                                <th>Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach( $news->comments as $cmt )
                            <tr class="odd gradeX" align="center">
                                <td>{{ $cmt->id }}</td>
                                <td>{{ $cmt->user->name }}</td>
                                <td>{{ $cmt->NoiDung }}</td>
                                <td>{{ $cmt->created_at }}</td>
                                <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a href="{{ route('comments.delete', ['id' => $cmt->id, 'idNews' => $news->id]) }}"> Delete</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@endsection

@section('script')
    <script>
        $(document).ready(function (params) {
            $('#TheLoai').change(function () {
               var idTheLoai = $(this).val();
               $.get("admin/ajax/messagetype/"+idTheLoai, function (data) {
                    $('#LoaiTin').html(data); 
               });
            });
        });
    </script>
    
@endsection