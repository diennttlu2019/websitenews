<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Comment;
use App\MessageType;

class News extends Model
{
    protected $table = "tintuc";
    protected $fillable = [
        'TieuDe',
        'TieuDeKhongDau',
        'TomTat',
        'NoiDung',
        'Hinh',
        'NoiBat',
        'SoLuotXem',
        'idLoaiTin'
    ];

    public function messageType()
    {
    	return $this->belongsTo(MessageType::class, 'idLoaiTin', 'id');
    }

    public function comments()
    {
  		return $this->hasMany(Comment::class, 'idTinTuc', 'id');
    }
}
