<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NewsCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'LoaiTin' => 'required',
            'TomTat' => 'required',
            'NoiDung' => 'required',
            'Hinh' => 'mimes:jpeg,jpg,png,',
        ];
        if ($this->has('id'))
        {
            $rules += ['TieuDe' => 'required|min:3|unique:tintuc,tieude,'.$id];
        } else {
            $rules += ['TieuDe' => 'required|min:3|unique:tintuc,tieude'];
        }
        return $rules;
    }



    public function messages()
    {
        return [
            'LoaiTin.required' => 'Bạn chưa chọn loại tin',
            'TieuDe.required' => 'Bạn chưa nhập tiêu đề',
            'TieuDe.min' => 'Tiêu đề phải ít nhất 3 kí tự',
            'TieuDe.unique' => 'Tiêu đề đã tồn tại',
            'TomTat.required' => 'Bạn chưa nhập tóm tắt',
            'NoiDung.required' => 'Bạn chưa nhập nội dung',
        ];
    }
}
