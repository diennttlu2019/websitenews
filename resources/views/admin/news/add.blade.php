@extends('admin.layout.index')
@section('content')
<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Tin tức
                            <small>Thêm tin tức</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-9" style="padding-bottom:120px">
                        @if(count($errors) > 0)
                            <div class="alert alert-danger">
                                @foreach($errors->all() as $err)
                                    {{$err}}<br>
                                @endforeach
                            </div>
                        @endif
                        @if( session('message') )
                            <div class="alert alert-success">
                                {{ session('message') }}
                            </div>
                        @endif
                        <form action="{{ route('news.add') }}" method="POST" enctype="multipart/form-data"> 
                            @csrf
                            <div class="form-group">
                                <label>Thể loại</label>
                                <select class="form-control" id="TheLoai">
                                    <option>Chọn thể loại</option>
                                    @foreach( $categories as $cat )
                                        <option value="{{ $cat->id }}">{{ $cat->Ten }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Loại tin</label>
                                <select class="form-control" name="LoaiTin" id="LoaiTin">
                                    <option>Chọn loại tin</option>
                                    
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Tiêu đề</label>
                                <input class="form-control" name="TieuDe" placeholder="Nhập tiêu đề tin tức" />
                            </div>
                            <div class="form-group">
                                <label>Tóm tắt</label>
                                <textarea id="TomTat" name="TomTat"  class="form-control ckeditor" rows="4"></textarea>
                            </div>
                            <div class="form-group">
                                <label>Nội dung</label>
                                <textarea id="NoiDung" name="NoiDung"  class="form-control ckeditor" rows="10"></textarea>
                            </div>
                            
                            <div class="form-group">
                                <label>Hình ảnh</label>
                                <input type="file" name="Hinh" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Nổi bật</label>
                                <label class="radio-inline">
                                    <input name="NoiBat" value="1" checked="" type="radio">Có
                                </label>
                                <label class="radio-inline">
                                    <input name="NoiBat" value="0" type="radio">Không
                                </label>
                            </div>
                            <input type="submit" class="btn btn-default" value="Thêm">
                            <input type="reset" class="btn btn-default" value="Làm mới">
                        <form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@endsection

@section('script')
    <script>
        $(document).ready(function (params) {
            $('#TheLoai').change(function () {
               var idTheLoai = $(this).val();
               $.get("admin/ajax/messagetype/"+idTheLoai, function (data) {
                    $('#LoaiTin').html(data); 
               });
            });
        });
    </script>
    
@endsection