@extends('admin.layout.index')
@section('content')
 <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Danh sách
                            <small>Tin tức</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    @if(session('message'))
                        <div class="alert alert-success">
                            {{session('message')}}
                        </div>
                    @endif
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                <th>ID</th>
                                <th>Tiêu đề</th>
                                <th>Tóm tắt</th>
                                <th>Thể loại</th>
                                <th>Loại tin</th>
                                <th>Xem</th>
                                <th>Nổi bật</th>
                                <th>Delete</th>
                                <th>Edit</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($news as $key => $n)
                            <tr class="odd gradeX" align="center">
                                <td>{{ $key+1 }}</td>
                                <td>
                                    <p>{{ $n->TieuDe }}</p>
                                    <img src="upload/tintuc/{{ $n->Hinh }}" width="100px"/>
                                </td>
                                <td>{{ $n->TomTat }}</td>
                                <td>{{ $n->messageType->category->Ten }}</td>
                                <td>{{ $n->messageType->Ten }}</td>
                                <td>{{ $n->SoLuotXem }}</td>
                                <td>
                                    @if($n->NoiBat == 0)
                                        {{ 'Không' }}
                                    @else
                                        {{ 'Có' }}
                                    @endif
                                </td>
                                <td class="center"><i class="fa fa-trash-o  fa-fw"></i>
                                <a href="{{ route('news.delete', ['id' => $n->id]) }}"> Delete</a>
                                </td>
                                <td class="center"><i class="fa fa-pencil fa-fw"></i> 
                                <a href="{{ route('news.edit', ['id' => $n->id]) }}">Edit</a></td>
                            </tr> 
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@endsection