<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MessageType;
use App\Category;
use App\Http\Requests\MessageTypeCreateRequest;

class MessageTypeController extends Controller
{
	public function getList()
	{
		$message_types = MessageType::orderBy('id', 'DESC')->get();
		return view('admin.messagetype.list', compact('message_types'));
	}

	public function getAdd()
	{
		$categories = Category::all();
		return view('admin.messagetype.add',compact('categories'));
	}

	public function postAdd(MessageTypeCreateRequest $request)
	{
		$message_type = MessageType::create([
			'idTheLoai' 	=> $request->idTheLoai,
			'Ten'			=> $request->Ten,
			'TenKhongDau'	=> changeTitle($request->Ten)
		]);
		return redirect()->back()->with('message', 'Thêm thành công');
	}

	public function getEdit($id)
	{
		$categories = Category::all();
		$message_type = MessageType::find($id);
		return view('admin.messagetype.edit', compact('categories', 'message_type'));
	}

	public function postEdit(MessageTypeCreateRequests $request, $id)
	{
		$message_type = MessageType::where('id', $id)->update([
			'idTheLoai' => $request->idTheLoai,
			'Ten' => $request->Ten,
			'TenKhongDau' => changeTitle($request->Ten),
		]);
		return redirect()->back()->with('message','Sửa thành công.');
	}

	public function getDelete($id)
	{
		$message_type = MessageType::find($id);
		$message_type->delete();
		return redirect()->route('messagetypes.list')->with('message', 'Đã xóa thành công.');
	}
}
