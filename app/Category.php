<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = "theloai";
    protected $fillable = ['Ten', 'TenKhongDau'];

    public function messageTypes()
    {
    	return $this->hasMany('App\MessageType', 'idTheLoai', 'id');
    }

    public function news()
    {
    	return $this->hasManyThrough('App\News', 'App\MessageType', 'idTheLoai', 'idLoaiTin', 'id');
    }
}
