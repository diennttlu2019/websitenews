<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('index', function () {
    return view('admin.index');
})->name('admin.index');

Route::group(['prefix' => 'admin'], function ()
{
	Route::group(['prefix' => 'categories'], function() {
		Route::get('list', 'CategoryController@getList')->name('categories.list');
		
	    Route::get('edit/{id}', 'CategoryController@getEdit')->name('categories.edit');
		Route::post('edit/{id}', 'CategoryController@postEdit')->name('categories.edit');
		
		Route::get('delete/{id}', 'CategoryController@getDelete')->name('categories.delete');
		
	    Route::get('add', 'CategoryController@getAdd')->name('categories.add');
	    Route::post('add', 'CategoryController@postAdd')->name('categories.add');
	});

	Route::group(['prefix' => 'messagetypes'], function() {
		Route::get('list', 'MessageTypeController@getList')->name('messagetypes.list');
		
	    Route::get('edit/{id}', 'MessageTypeController@getEdit')->name('messagetypes.edit');
		Route::post('edit/{id}', 'MessageTypeController@postEdit')->name('messagetypes.edit');
		
		Route::get('delete/{id}', 'MessageTypeController@getDelete')->name('messagetypes.delete');
		
	    Route::get('add', 'MessageTypeController@getAdd')->name('messagetypes.add');
	    Route::post('add', 'MessageTypeController@postAdd')->name('messagetypes.add');
	});

	Route::group(['prefix' => 'news'], function() {
		Route::get('list', 'NewsController@getList')->name('news.list');

		Route::get('edit/{id}', 'NewsController@getEdit')->name('news.edit');
		Route::post('edit/{id}', 'NewsController@postEdit')->name('news.edit');

		Route::get('delete/{id}', 'NewsController@getDelete')->name('news.delete');

		Route::get('add', 'NewsController@getAdd')->name('news.add');
		Route::post('add', 'NewsController@postAdd')->name('news.add');
	});

	Route::group(['prefix' => 'comments'], function() {
		Route::get('delete/{id}/{idNews}', 'CommentController@getDeleteComment')->name('comments.delete');
	});

	Route::group(['prefix' => 'slides'], function() {
		Route::get('list', 'SlideController@getList')->name('slides.list');
		
		Route::get('edit', 'SlideController@getEdit')->name('slides.edit');
		Route::post('edit', 'SlideController@postEdit')->name('slides.edit');
		
		Route::get('add', 'SlideController@getAdd')->name('slides.add');
		Route::post('add', 'SlideController@postAdd')->name('slides.add');

		Route::get('delete', 'SlideController@getDelete')->name('slides.delete');
	});

	Route::group(['prefix' => 'users'], function () {
		Route::get('list', 'UserController@getList')->name('users.list');
		
		Route::get('edit', 'UserController@getEdit')->name('users.edit');

		Route::get('add', 'UserController@getAdd')->name('users.add');

		Route::get('delete', 'UserController@getDelete')->name('users.delete');
	});

	Route::group(['prefix' => 'ajax'], function () {
		Route::get('messagetype/{idTheLoai}', 'AjaxController@getMessageType')->name('ajax.messagetype');
	});
});

