<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoryCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'Ten' => 'required|min:3|max:100|unique:theloai,ten,'.$id
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'Ten.required' => 'Bạn chưa nhập tên thể loại',
            'Ten.unique' => 'Thể loại đã tồn tại.',
            'Ten.min' => 'Thể loại phải có độ dài từ 3 đến 100 ký tự',
            'Ten.max'=> 'Thể loại phải có độ dài từ 3 đến 100 ký tự',
        ];
    }
}
