<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Http\Requests\CategoryCreateRequest;

class CategoryController extends Controller
{
   	public function getList()
   	{
         $categories = Category::orderBy('id', 'DESC')->get();
   		return view('admin.category.list', compact('categories'));
   	}

      public function getAdd()
      {
         return view('admin.category.add');
      }

      public function postAdd(CategoryCreateRequest $request)
      {
         $category = Category::create([
            'Ten'          => $request->Ten,
            'TenKhongDau'  => changeTitle($request->Ten)
         ]);
         if ($category) {
            return redirect()->back()->with('message_success','Thêm thành công');
         } else {
            return redirect()->back()->with('message_error','Thêm không thành công');
         }
      }

   	public function getEdit($id)
   	{
   		$category = Category::find($id);
         return view('admin.category.edit',['category' => $category]);
   	}

      public function postEdit(CategoryCreateRequest $request, $id)
      {
         $category = Category::where('id', $id)->update([
            'Ten'          => $request->Ten,
            'TenKhongDau'  => changeTitle($request->Ten)
         ]);
         return redirect()->back()->with('message','Sửa thành công.');
      }

      public function getDelete($id)
      {
         $category = Category::find($id);
         $category->delete();
         return redirect()->route('categories.list')->with('message', 'Xóa thành công');
      }
}
