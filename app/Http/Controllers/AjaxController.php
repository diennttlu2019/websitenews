<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;
use App\Category;
use App\MessageType;

class AjaxController extends Controller
{
    public function getMessageType($idTheLoai)
    {
        $message_types = MessageType::where('idTheLoai', $idTheLoai)->get();
        foreach ($message_types as $ms) {
            echo "<option value='".$ms->id."'>".$ms->Ten."</option>";
        }
    }
}
